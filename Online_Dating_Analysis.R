library(dplyr) # Data manipulation
library(reshape)
library(reshape2) # Data reshaping for ggplot
library(ggplot2) # Data visualization
library(plotly) # Dynamic data visualization
library(RColorBrewer) # Colors on plots
library(readr) # CSV file I/O, e.g. the read_csv function
library(dataQualityR) # DQR generation
library(randomForest) # Random Forest for variable importance
#Color palettes for plots
#decide to change the color of the ggplot afterwards, it could be useful.
cbPalette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
cbbPalette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
#Read the data
missing.types <- c("NA", "")
df <- read.csv("C:\\Users\\Me\\Documents\\VIT\\Projects\\SIN Project\\Speed Dating Data.csv", na.strings = missing.types, stringsAsFactors = F)
# "income", "tuition" and "mn_sat" features should be read as numerical
df$income <- as.numeric(gsub(",", "", df$income))
df$tuition <- as.numeric(gsub(",", "", df$tuition))
df$mn_sat <- as.numeric(gsub(",", "", df$mn_sat))
#Cleaning the Data
#Generate the Data Quality Report, before the feature engineering
#DQR feature has the most missing values? How many unique values are present for this or this feature? Etc.
#It is a very good help to understand and clean the data.
#It appears that nothing very interesting can be deducted from this. Indeed,
#most of the missing values are preferences of the people considered. Impossible to impute that!
sample_n(df[is.na(df$imprelig), c(1:10)], 10)
missing.age_o.pid <- unique(df[is.na(df$age_o),]$pid)
missing.race_o.pid <- unique(df[is.na(df$race_o),]$pid)
sample_n(subset(df, iid %in% missing.age_o.pid)[, c(1:10)], 10)
# Find the iid of the only missing id
iid <- df[is.na(df$id),]$iid
# Assign this iid' id to the missing iid..
df[is.na(df$id),]$id <- head(df[df$iid == iid,]$id, 1)
# Show the missing pid
#Deal with pid (partner's iid number) in the dataset.
df[is.na(df$pid), c(1,2,11,12)]
# Save the partner number for the wave
partner.pid <- unique(df[is.na(df$pid),]$partner)
# Save the wave number
wave.pid <- unique(df[is.na(df$pid),]$wave)
# Show the iid we are looking for
unique(df[df$wave == wave.pid & df$id == partner.pid,]$iid)
df[is.na(df$pid),]$pid <- 128
#Work on field feature
# Plot raw field
df$field <- tolower(df$field)
barplot(
  table(df$field),
  main = "Careers"
)
# How many unique field do we have?
paste("There is", length(unique(df$field)), "different uncoded fields.")
#We cannot use this column to analyse the data. Indeed, all the values
#in this column were directly given by the users, and the latter may write anything,
#including nonsense values such as "I don't know".
# Show coded field
ordered(unique(df$field_cd))
#Work on career feature
# Plot raw career
df$career <- tolower(df$career)
barplot(
  table(df$career),
  main = "Careers"
)
# How many unique carrer do we have?
paste("There is", length(unique(df$career)), "different uncoded carreers.")
# Show coded career (should have 17 levels)
ordered(unique(df$career_c))
# Assign NA to all the zip codes equals to 0
df[df$zipcode == 0 & !is.na(df$zipcode),]$zipcode <- NA
#It is said on the word doc linked to the data that the waves 6 to 9
#are different because people were asked to note their preferences from 1 to 10 rather
#than allocating a hundred points on features.
# Show waves 6 to 9
head(df[df$wave > 5 & df$wave < 10, c(1,16:20)])
# Show wave 3 to compare
head(df[df$wave == 3, c(1,16:20)])
#already normalised
#Change Male and Female attributes
df[df$gender == 0,]$gender <- "W"
df[df$gender == 1,]$gender <- "M"
write.csv(df, "df-clean.csv")
#ANALYSIS
# 1.GENDER ANALYS
#How many women/men in the experiments
df %>%
  group_by(iid) %>%
  summarise(gender = head(gender,1)) %>%
  group_by(gender) %>%
  summarise(my.n = n())
#dont consider duplicates
gender.rep.over.waves <- subset(df, !duplicated(df[, 1])) %>%
  group_by(wave, gender) %>%
  summarise(my.n = n()) %>%
  melt(id.vars = c("gender", "wave"))
# Plot gender repartition in waves
ggplot(gender.rep.over.waves, aes(x = wave, y = value, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Wave") + ylab("Population") + ggtitle("Gender repartition in waves") +
  scale_fill_manual(values = cbPalette)
# 2.Age analysis
age.rep.over.waves <- subset(df, !duplicated(df[, 1])) %>%
  filter(!is.na(age)) %>%
  group_by(wave, gender) %>%
  summarise(my.m = mean(age)) %>%
  melt(id.vars = c("gender", "wave"))
# Plot age repartition in waves
ggplot(age.rep.over.waves, aes(x = wave, y = value, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Wave") + ylab("Mean age") + ggtitle("Age repartition in waves") +
  scale_fill_manual(values = cbPalette)
# What are the extremums?
print(paste0("Minimum age in all the dataset is: ", min(df[!is.na(df$age),]$age)))
print(paste0("Max age in all the dataset is: ", max(df[!is.na(df$age),]$age)))
print(paste0("Mean age in all the dataset is: ", mean(df[!is.na(df$age),]$age)))
# 3.Work and studies analysis
# Create study field codes
fields.cd <- c(
  "Law",
  "Math",
  "Social Science, Psychologist" ,
  "Medical Science, Pharmaceuticals, and Bio Tech",
  "Engineering",
  "English/Creative Writing/ Journalism",
  "History/Religion/Philosophy",
  "Business/Econ/Finance",
  "Education, Academia",
  "Biological Sciences/Chemistry/Physics",
  "Social Work" ,
  "Undergrad/undecided" ,
  "Political Science/International Affairs" ,
  "Film",
  "Fine Arts/Arts Administration",
  "Languages",
  "Architecture",
  "Other"
)
# Create career codes
career.cd <- c(
  "Lawyer",
  "Academic/Research",
  "Psychologist",
  "Doctor/Medicine",
  "Engineer",
  "Creative Arts/Entertainment",
  "BankingBusiness/CEO/Admin",
  "Real Estate",
  "International/Humanitarian Affairs",
  "Undecided" ,
  "Social Work",
  "Speech Pathology",
  "Politics",
  "Pro sports/Athletics",
  "Other",
  "Journalism",
  "Architecture"
)
# Find number of men/women on each study field
fields <- df[!is.na(df$field_cd),] %>%
  group_by(gender, field_cd) %>%
  summarise(
    my.n = n()
  )
# Plot study fields repartition
ggplot(fields, aes(x = field_cd, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Field") + ylab("Count") + ggtitle("Study fields repartition") +
  scale_x_continuous(labels = fields.cd, breaks = 1:18) +
  coord_flip()
# Find number of men/women on each career
careers <- df[!is.na(df$career_c),] %>%
  group_by(gender, career_c) %>%
  summarise(
    my.n = n()
  )
# Plot careers repartition
ggplot(careers, aes(x = career_c, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Career") + ylab("Count") + ggtitle("Careers repartition") +
  scale_x_continuous(labels = career.cd, breaks = 1:17) +
  coord_flip()
# 4.Race analysis
# Create race code
race.c <- c(
  "Black/African American",
  "European/Caucasian-American",
  "Latino/Hispanic American",
  "Asian/Pacific Islander/Asian-American",
  "Native American",
  "Other"
)
# Find number of men/women for each race
races <- df[!is.na(df$race),] %>%
  group_by(gender, race) %>%
  summarise(
    my.n = n()
  )
# Plot race repartition
ggplot(races, aes(x = race, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Race") + ylab("Count") + ggtitle("Race repartition") +
  scale_x_continuous(labels = race.c, breaks = 1:6) +
  coord_flip()
#5.Goal analysis
# Create goal code
goal.c <- c(
  "Seemed like a fun night out",
  "To meet new people",
  "To get a date",
  "Looking for a serious relationship",
  "To say I did it",
  "Other"
)
# Find number of men/women for each goal
goals <- df[!is.na(df$goal),] %>%
  group_by(gender, goal) %>%
  summarise(
    my.n = n()
  )
# Plot goals repartition
ggplot(goals, aes(x = goal, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Goal") + ylab("Count") + ggtitle("Goal repartition") +
  scale_x_continuous(labels = goal.c, breaks = 1:6) +
  coord_flip()
#6.Date & go out analysis
# Create date & go out code
date.c <- c(
  "Several times a week",
  "Twice a week",
  "Once a week",
  "Twice a month",
  "Once a month",
  "Several times a year",
  "Almost never"
)
# Find date occurrence for men/women
dates <- df[!is.na(df$date),] %>%
  group_by(gender, date) %>%
  summarise(
    my.n = n()
  )
# Plot dates repartition
ggplot(dates, aes(x = date, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Date") + ylab("Count") + ggtitle("Date repartition") +
  scale_x_continuous(labels = date.c, breaks = 1:7) +
  coord_flip()
# Find go out occurrence for men/women
go.outs <- df[!is.na(df$go_out),] %>%
  group_by(gender, go_out) %>%
  summarise(
    my.n = n()
  )
# Plot go out repartition
ggplot(go.outs, aes(x = go_out, y = my.n, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Go out") + ylab("Count") + ggtitle("Go out repartition") +
  scale_x_continuous(labels = date.c, breaks = 1:7) +
  coord_flip()
#7.Matches analysis
# Dummie analysis on Matches
barplot(
  table(df$match),
  main = "Matches proportion",
  col = "black"
)
#Match by wave analysis
match.by.waves <- df[df$match == 1,] %>%
  group_by(wave) %>%
  summarise(
    nb_matches = sum(match == 1)
  )
# Plot matches for waves: what was the best wave to be?
ggplot(match.by.waves, aes(x = wave, y = nb_matches)) +
  geom_bar(stat = "identity", position = "dodge") + ggtitle("Matches by waves") +
  xlab("Wave number") + ylab("Matches")
#Match vs dec
df %>%
  group_by(match, dec) %>%
  summarise(
    my.n = n()
  ) %>%
  ggplot(aes(x = dec, y = my.n, fill = factor(match))) +
  geom_bar(stat = "identity", position = "dodge") + ggtitle("Match vs dec") +
  xlab("Decision") + ylab("Count")
#It is interesting to notice that some people wanted to meet again their partner even if there was
#not a match between them - more than 2000 case in this dataset.
#8. Look for best attributes to have
# For men
#Preminilary analysis
# Isolate the men from the dataset
men <- df[df$gender == "M",]
# Get first and last index of the columns of the features we are interested in
first.col.index <- head(grep("sports", colnames(df)), 1)
last.col.index <- head(grep("yoga", colnames(df)), 1)
# Get index of the `match` column
match.col.index <- head(grep("match", colnames(df)), 1)
# Create vector without NAs to use forest
men <- men[complete.cases(men[first.col.index:last.col.index]),]
# Group men by iid
men.grouped.iid <- men %>%
  group_by(iid) %>%
  summarise(
    sum.match = sum(match)
  )
# Find the number of men per number of matches
n.match.men <- men.grouped.iid %>%
  group_by(sum.match) %>%
  summarise(
    my.n = n()
  )
# Plot number of match per man
ggplot(n.match.men, aes(x = sum.match, y = my.n)) +
  geom_bar(stat = "identity", position = "dodge", fill="#E69F00", colour="black") +
  xlab("Number of matches") + ylab("Count") + ggtitle("Number of men per number of matches")
#Simple Random Forest classifier
# Set the random seed to make this result reproducible
set.seed(50)
# Feed a randomForest model
fit <- randomForest(as.factor(match) ~ sports + tvsports + exercise +
                      dining + museums + art + hiking + music + gaming + clubbing +
                      reading + tv + theater + movies + concerts + shopping + yoga,
                    data = men,
                    importance=TRUE,
                    ntree=2000
)
# Get the importance of the features
# We need to perform several operations on the fit$importance field, including:
# - take only the column we are interested in,
# - create a new column with the rowname on it,
# - rename the columns.
importance.features <- tibble::rownames_to_column(data.frame(fit$importance[,c(1)]))
colnames(importance.features) <- c("rowname", "value")
# Plot the importance of the features for a man
ggplot(importance.features, aes(x = reorder(rowname, -value), y = value)) +
  geom_bar(stat = "identity", position = "dodge", fill="#E69F00", colour="black") +
  xlab("Feature") + ylab("Count") + ggtitle("Importance of a feature: Simple Random Forest classifier") +
  coord_flip()
#How to get more than 5 matches?
# Set the random seed to make this result reproducible
set.seed(999)
# Get the iid of the person with more than 5 matches
more.than.5 <- unique(men.grouped.iid[men.grouped.iid$sum.match > 5,]$iid)
# Feed a randomForest model
fit <- randomForest(as.factor(match) ~ sports + tvsports + exercise +
                      dining + museums + art + hiking + music + gaming + clubbing +
                      reading + tv + theater + movies + concerts + shopping + yoga,
                    data = subset(men, iid %in% more.than.5), # Subsetting to only get the matchs > 5
                    importance=TRUE,
                    ntree=5000
)
# Get the importance of the features
importance.features <- tibble::rownames_to_column(data.frame(fit$importance[,c(1)]))
colnames(importance.features) <- c("rowname", "value")
# Plot the importance of the features for a man
ggplot(importance.features, aes(x = reorder(rowname, -value), y = value)) +
  geom_bar(stat = "identity", position = "dodge", fill="#E69F00", colour="black") +
  xlab("Feature") + ylab("Count") + ggtitle("Importance of a feature: how to get more than 5 matches?") +
  coord_flip()
#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#for women
#Preminilary analysis
# Isolate the women from the dataset
women <- df[df$gender == "W",]
# Create vector without NAs to use forest
women <- women[complete.cases(women[first.col.index:last.col.index]),]
# Group women by iid
women.grouped.iid <- women %>%
  group_by(iid) %>%
  summarise(
    sum.match = sum(match)
  )
# Find the number of women per number of matches
n.match.women <- women.grouped.iid %>%
  group_by(sum.match) %>%
  summarise(
    my.n = n()
  )
# Plot number of match per man
ggplot(n.match.women, aes(x = sum.match, y = my.n)) +
  geom_bar(stat = "identity", position = "dodge", fill="#56B4E9", colour="black") +
  xlab("Number of matches") + ylab("Count") + ggtitle("Number of women per number of matches")
# Isolate women with matches
women.matches <- df[df$gender == "W" & df$match == 1,]
#random forest
# Set the random seed to make this result reproducible
set.seed(50)
# Feed a randomForest model
fit <- randomForest(as.factor(match) ~ sports + tvsports + exercise +
                      dining + museums + art + music + hiking + gaming + clubbing +
                      reading + tv + theater + movies + concerts + shopping + yoga,
                    data = women,
                    importance=TRUE,
                    ntree=2000
)
# Get the importance of the features
importance.features <- tibble::rownames_to_column(data.frame(fit$importance[,c(1)]))
colnames(importance.features) <- c("rowname", "value")
# Plot the importance of the features for a man
ggplot(importance.features, aes(x = reorder(rowname, -value), y = value)) +
  geom_bar(stat = "identity", position = "dodge", fill="#56B4E9", colour="black") +
  xlab("Feature") + ylab("Count") + ggtitle("Importance of a feature: Simple Random Forest classifier") +
  coord_flip()
# How to get more than 5 matches?
# Set the random seed to make this result reproducible
set.seed(999)
# Get the iid of the person with more than 5 matches
more.than.5 <- unique(women.grouped.iid[women.grouped.iid$sum.match > 5,]$iid)
# Feed a randomForest model
fit <- randomForest(as.factor(match) ~ sports + tvsports + exercise +
                      dining + museums + art + hiking + music + gaming + clubbing +
                      reading + tv + theater + movies + concerts + shopping + yoga,
                    data = subset(women, iid %in% more.than.5), # Subsetting to only get the matchs > 5
                    importance=TRUE,
                    ntree=5000
)
# Get the importance of the features
importance.features <- tibble::rownames_to_column(data.frame(fit$importance[,c(1)]))
colnames(importance.features) <- c("rowname", "value")
# Plot the importance of the features for a man
ggplot(importance.features, aes(x = reorder(rowname, -value), y = value)) +
  geom_bar(stat = "identity", position = "dodge", fill="#56B4E9", colour="black") +
  xlab("Feature") + ylab("Count") + ggtitle("Importance of a feature: how to get more than 5 matches?") +
  coord_flip()
#9.Does race really matters?
# Find samerace importance on imprace
imprace.importance <- df %>%
  group_by(samerace, imprace) %>%
  summarise(
    sum.match = sum(match),
    total = n()
  )
# Eliminate imprace = 0 because the scale is from 1 to 10
imprace.importance <- imprace.importance[imprace.importance$imprace > 0 & !is.na(imprace.importance$imprace),]
# Plot samerace over imprace
ggplot(imprace.importance, aes(x = imprace, y = (sum.match / total) * 100, fill = factor(samerace))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Samerace") +
  xlab("Imprace") + ylab("Count (%)") + ggtitle("Importance of `imprace`") +
  scale_fill_manual(values = cbPalette)
#10.Does age really matter?
# Group df by age and age_0
age.analysis <- df %>%
  group_by(age, age_o) %>%
  summarise(
    n.people = n(),
    n.matches = sum(match)
  ) %>%
  filter(!is.na(age) & !is.na(age_o))
# Filter with age difference > 5 years, and with more than 5 matches
age.diff <- age.analysis %>%
  filter(age - age_o >= 0) %>%
  mutate(n.years = age - age_o) %>%
  group_by(n.years) %>%
  summarise(
    n.matches = sum(n.matches)
  ) %>%
  arrange(n.years)
# Graph result
ggplot(age.diff[age.diff$n.years < 20,], aes(x = n.years, y = n.matches)) +
  geom_bar(stat = "identity", position = "dodge") +
  xlab("Number of years of difference between people's age") +
  ylab("Number of matches") + ggtitle("Does age really matter?") +
  scale_fill_manual(values = cbPalette)
#11.Does correlation between participant's and partner's ratings of interests really matter?
# Group df with int_corr and gender
int_corr.analysis <- df %>%
  group_by(int_corr) %>%
  summarise(
    my.n = n(),
    n.matches = sum(match)
  ) %>%
  filter(!is.na(int_corr))
int_corr.analysis$n.matches.cat[int_corr.analysis$n.matches >= 0 &
                                  int_corr.analysis$n.matches < 10] <- "few"
int_corr.analysis$n.matches.cat[int_corr.analysis$n.matches >= 10 &
                                  int_corr.analysis$n.matches < 20] <- "middle"
int_corr.analysis$n.matches.cat[int_corr.analysis$n.matches >= 20] <- "lot"
# Plot result
ggplotly(ggplot(int_corr.analysis, aes(x = int_corr, y = my.n, fill = factor(n.matches.cat))) +
           geom_bar(stat = "identity", position = "dodge") +
           scale_fill_discrete(name = "Matches") +
           xlab("int_cor") + ylab("Count") +
           ggtitle(paste("Does correlation between participant's and partner's \n",
                         "ratings of interests really matter? \n", sep = "<br>")))
#12.Are people more willing to meet someone working in the same field?
# Temp variable to store `pid`
temp <- df$pid
# Blank list to store the partner coded career
my.career_c_o <- list()
# Loop to create the column we need in df
# The goal is to add the coded career of the person met to each row
for(i in 1:length(temp)){
  my.career_c_o[i] <- head(df[df$iid == temp[i],]$career_c, 1)
}
# Copy df to avoid working on the real data
df.copy <- df
# Add the coded field of partner
df.copy$career_c_o <- unlist(my.career_c_o)
# Explore...
var1 <- df.copy %>%
  filter(match == 1) %>%
  group_by(career_c, career_c_o) %>%
  summarise(my.n = n()) %>%
  filter(!is.na(career_c) & !is.na(career_c_o))
# Heatmap to display result
ggplotly(ggplot(var1, aes(career_c, career_c_o)) +
           geom_tile(aes(fill = my.n), colour = "white") +
           scale_fill_gradient(low = "white", high = "steelblue") +
           ggtitle(paste("Are people more willing to meet \n",
                         "someone working in the same field? \n", sep = "<br>")))
#13.Is income important to get more matches?
# Dummie analysis: group by match and see extremums, mean, median...
df %>%
  filter(!is.na(income)) %>%
  group_by(match, gender) %>%
  summarise(
    mean = mean(income),
    median = median(income),
    max = max(income),
    min = min(income),
    n.matches = sum(match)
  )
#14.Is it important to be on the firsts dates to have match?
df %>%
  filter(match == 1) %>%
  group_by(order, gender) %>%
  summarise(
    n.matches = sum(match)
  ) %>%
  ggplot(aes(x = order, y = n.matches, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +
  xlab("Order") + ylab("Number of matches") +
  ggtitle("Is it important to be on the firsts dates to have match?")
df %>%
  filter(match == 1) %>%
  group_by(order, gender) %>%
  summarise(
    n.matches = sum(match)
  ) %>%
  ggplot(aes(x = order, y = n.matches, fill = factor(gender))) +
  geom_bar(stat = "identity", position = "dodge") +
  scale_fill_discrete(name = "Gender") +